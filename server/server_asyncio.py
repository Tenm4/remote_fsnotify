#! /usr/bin/env python3
# coding: utf-8

import asyncio
import os
import ssl
import struct
from datetime import datetime
from pathlib import Path
import traceback

from config import load_config

config = load_config()

class RemoteFSNotifyProtocol(asyncio.Protocol):

    def __init__(self):
        self.outfile = None
        self.expected_data_len = 0
        self.buf = b""

    def connection_made(self, transport):
        self.peername = transport.get_extra_info('peername')
        print('Connection from {}'.format(self.peername))
        self.transport = transport

    def connection_lost(self, exc):
        print("Reason for losing connection:", exc if exc else "EOF")
        self.eof_received()
            
    def eof_received(self):
        if self.outfile:
            self.outfile.close() # Noop if already closed

    def create_target_file(self, path):
        # Create a simple the directory hierarchy
        peer_directory = os.path.join(config['target_root_path'], self.peername[0])
        os.makedirs(peer_directory, exist_ok=True)

        # Timestamp for client files for versioning 
        path = f"{path}_{datetime.now().strftime('%Y%m%d%H%M%S')}"
        self.outfile = open(os.path.join(peer_directory, path), 'wb')

    def data_received(self, data):
        print('RECV {}'.format(len(data)), end=' ')
        # print(data)

        self.buf += data
        while self.buf:
            try:
                self.RFSN_process()
            except (IndexError, struct.error) as e:
                # Custom processing error would be better than (IndexError, struct.error)
                # Silence exception because expected: used as control flow
                # print("Not enough data to process further. Error was:")
                # traceback.format_exc()
                pass
            break

    def RFSN_decode_metadata(self):
        """Decode RFSN protocol metadata and return it along with number of processed bytes.
        Format: (PDU_type, bytes_processed, (type-specific metadata))
        Raise struct.error if not enough data to process."""
        buf = self.buf
        typ, = struct.unpack(">4s", buf[:4])
        typ = typ.decode("UTF-8")
        if typ == "TEST":
            return (typ, 4, ())
        elif typ == "FILE":
            contents_len, path_len = struct.unpack(">QB", buf[4:13])
            path, = struct.unpack(f">{path_len}s", buf[13 : 13 + path_len])
            return (typ, 13 + path_len, (path, contents_len))
        elif typ == "DATA":
            data_len, = struct.unpack(">H", buf[4:6])
            return (typ, 6, (data_len))
        elif typ == "*END":
            return (typ, 4, ())
        return ("NONE", 1, ())

    def RFSN_process(self):
        """Process RFSN protocol data based on its metadata.
        Raise IndexError if not enough data to process"""
        typ, bytes_processed, extra = self.RFSN_decode_metadata()
        # print(typ, "hprocessed", bytes_processed, "extra", extra)

        buf = self.buf[bytes_processed:]
        if typ == "FILE":
            print("Transfer start: recieved metadata")
            path, contents_len = extra

            self.create_target_file(path.decode("UTF-8", errors="replace"))

            self.expected_data_len = contents_len
        elif typ == "DATA":
            data_len = extra

            self.outfile.write(buf[:data_len])
            self.outfile.flush()

            bytes_processed += data_len
            self.expected_data_len -= data_len
            if self.expected_data_len < 0:
                print("Recieved more data than expected: ", -self.expected_data_len)
        elif typ == "*END":
            if self.expected_data_len:
                print("Mismatch: End Of Transmission, remaining bytes > 0")
            else:
                print("Transfer successful")
        elif typ == "TEST":
            print("Test OK")
        else:
            print("Unknown message type:", typ)

        self.buf = buf[bytes_processed:]
        if len(self.buf) > 0:
            print("Remaining data in buffer")
        print(typ, "buflen", len(self.buf), "expect", self.expected_data_len, "tprocd", bytes_processed)


if __name__ == "__main__":
    # SSL context to have encrypted channel
    ssl_context = ssl.create_default_context(ssl.Purpose.CLIENT_AUTH)
    ssl_context.load_cert_chain(config['cert_path'],
                                config['privkey_path'])

    loop = asyncio.get_event_loop()
    # Each client connection will create a new protocol instance
    server = loop.run_until_complete(
        loop.create_server(
            RemoteFSNotifyProtocol,
            config['bind']['host'],
            config['bind']['port'],
            ssl=ssl_context
        )
    )

    # Serve requests until Ctrl+C is pressed
    print('Serving on {}'.format(server.sockets[0].getsockname()))
    try:
        loop.run_forever()
    except KeyboardInterrupt:
        pass

    # Tear down server
    server.close()
    loop.run_until_complete(server.wait_closed())
    loop.close()
