#! /usr/bin/env python3
# coding: utf-8

from yaml import safe_load, Loader
import os

CONFIG_FILE_PATH = './config.yml'

def load_config():
    assert os.path.isfile(CONFIG_FILE_PATH), 'Server config file is missing, be sure that config file is in the same directory'

    with open(CONFIG_FILE_PATH, 'r') as f:
        config = safe_load(f)

    # handle user home path expansion
    for field in ['target_root_path', 'cert_path', 'privkey_path']:
        config[field] = os.path.expanduser(config[field])

    return config
