# Synopsis

Client-server program to monitor and send files that were modified on remote filesystems.

# Components

- A Python server that wait for remote files to store by client and version;
- A Rust client that watch for and send files that have been written to;
- A pair of certificate/private key to provide security for the transfer.
  You can generate your own based on the provided CSR config file.

# Setup

## Server

A Pipfile is provided for use with pipenv system, and a requirements.txt for vanilla pip.

To install in a virtualenv:

```
$ pip install pipenv
$ pipenv shell --python 3.6
$ pipenv install
```

Or without:

```
$ pip install -r requirements.txt
```

## Client

A Cargo.toml is provided for use with the Cargo system.

To install dependencies and build the project for release (takes long):

```
$ cargo build --release
```

For development, use:

```
$ cargo check # only validate the program
$ cargo build # validate and build project
```

The executable is generated in `target/{debug,release}/remote-fsnotify`.

Rust by default statically links all libraries but the C platform ones (libc, libdl...). Statically linking those require more effort , has drawbacks, and may be unsupported on some systems. Refer to https://doc.rust-lang.org/1.5.0/book/advanced-linking.html. 

# Usage

## Server

Server config file is config.yml.
`target_root_path` is where to store recieved files.

Run with:

```
$ python3 server_asyncio.py
```

The program by default load certificate/privkey from a separate `certs` directory.

## Client

Run with:

```
$ ./remote-fsnotify $watch_path $ip:port
```

Or if located in the project hierarchy, to validate, build and run:

```
$ cargo run $watch_path $ip:port
``` 

The program loads the server certificate at compile time to embed into the executable. Edit source `main.rs` to change the cert path.

# Compatibility

Server needs Python >= 3.5 and is thus multiplatform.

Client is compatible Linux, Windows, MacOS. Limiting factor is dependency `notify` which only handles those 3.

# Design

## Security and reliability

Data security is handled on transit. 
Use of TLS provides transport confidentiality + integrity + server authentication.

As for reliability, the client is deemed much more reliable than the server.

For the server, mere usage of safe Rust guarantees memory safety, and using its typing system correctly leads to very limited unexpected outcomes. Principle applied: the program must panic on startup on any case of failure, but should not after that.

The client, as written with high-level mature Python should be free from low-level bugs. However, the lack of compile-time checks leaves entry for any logical bugs. Fortunately, small code means few non-trivial bugs. Network handling code is trivial; protocol implementation a bit less. Protocol decoding strategy is aggressive and any parsing error is purposedly used for flow control. Some cases are not handled.
With more time, typechecking with `mypy` and testing with `pytest` would have provided more guarantees.

## Protocol

Remote File System Notify Protocol is a very straitforward protocol.
It supports testing connection, sending file metadata (only filename) and sending arbitrary chunks of binary data.
It functions on top of TCP and TLS.

### Motivation

This protocol exists because of the lack of trivial, reliable and secure file transfer protocol. What is meant by this is:
- trivial: only nee to test connection and send file
- reliable and secure: it has transport confidentiality + integrity + server auth thanks to TLS. Used a robust cryptosystem because You Shall No Roll Your Own Crypto©.

An option would have been HTTPS but it is bulky for our small needs. Granted it would have saved time.

### Test connection:

type:         4B "TEST"

### File metadata:

type:         4B "FILE"
len_filename: 1B,
len_contents: 8B,
filename:     var_len < 2^8, 

### Raw data chunk:

type:         4B "DATA"
chunk_len:    2B
chunk:        var_len < 2^16

### Transfer end:

type:         4B "*END"


## Server

Libraries used:

- standard library asyncio low-level API
- standard library ssl module

Room for improvment:

- didn't encapsulate protocol code
- didn't used custom exceptions in protocol parsing

## Client

Libraries:

- notify: portable file system event notifier. Linux, Windows, MacOS
https://crates.io/crates/notify
- Rustls (use ring + webpki): Rust impl of TLS with no support for insecure parameters.
https://github.com/ctz/rustls
- webpki: Rust impl of PKI operations for certificate verification
https://crates.io/crates/webpki
- protocol, protocol-derive: custom protocol codec
https://crates.io/crates/protocol
- lazy_static: work around a limitation of current Rust to provide runtime-defined global variables, use with moderation
