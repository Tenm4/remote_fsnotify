
use protocol as lib_protocol;

// Semantically const, but cannot because not result of "constant function"
// Language shortcoming worked around using lazy_static crate
lazy_static! {
    static ref LIB_PROTOCOL_SETTINGS:lib_protocol::Settings = lib_protocol::Settings::default();
}

/* Protocol messages trait */

pub trait RFSNMsg: lib_protocol::Parcel {
    
    fn encode(self) -> Result<Vec<u8>, lib_protocol::Error> {
        self.raw_bytes(&LIB_PROTOCOL_SETTINGS)
    }
}

/* Protocol messages implementation */

// Using method new to set msg_type although it could be set as a const
// But language doesn't permit const or default fields in stucts

#[derive(Protocol, Debug, PartialEq)]
pub struct RFSNTest {
    msg_type: [u8; 4],
}

impl RFSNTest {
    
    pub fn new() -> Self {
        Self { msg_type: b"TEST".to_owned() }
    }
}
impl RFSNMsg for RFSNTest {}

#[derive(Protocol, Debug, PartialEq)]
pub struct RFSNFileHeader {
    msg_type: [u8; 4],
    pub contents_len: u64,
    pub filename_len: u8,
    #[protocol(length_prefix(bytes(filename_len)))]
    pub filename: String,
}

impl RFSNFileHeader {
    
    pub fn new(filename:&str, contents_len:u64) -> Self {
        assert!(filename.len() < u8::max_value() as usize);
        Self {
            msg_type: b"FILE".to_owned(),
            filename_len: filename.len() as u8,
            contents_len: contents_len,
            filename: filename.to_owned(),
        }
    }
}
impl RFSNMsg for RFSNFileHeader {}

#[derive(Protocol, Debug, PartialEq)]
pub struct RFSNDataChunk {
    msg_type: [u8; 4],
    pub chunk_len: u16,
    #[protocol(length_prefix(bytes(chunk_len)))]
    pub chunk: Vec<u8>,
}

impl RFSNDataChunk {
        
    pub fn new(chunk:&[u8]) -> Self {
        assert!(chunk.len() < u16::max_value() as usize);
        Self {
            msg_type: b"DATA".to_owned(),
            chunk_len: chunk.len() as u16,
            chunk: chunk.to_owned(),
        }
    }
}
impl RFSNMsg for RFSNDataChunk {}

#[derive(Protocol, Debug, PartialEq)]
pub struct RFSNEnd {
    msg_type: [u8; 4],
}

impl RFSNEnd {
    
    pub fn new() -> Self {
        Self { msg_type: b"*END".to_owned() }
    }
}
impl RFSNMsg for RFSNEnd {}
