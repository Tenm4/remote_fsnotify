extern crate notify;
extern crate protocol;
#[macro_use] extern crate protocol_derive;
#[macro_use] extern crate lazy_static;

use std::error::Error;
use std::env;
use std::fs::File;
use std::path::{Path, PathBuf};
use std::net::TcpStream;
use std::sync::Arc;
use std::io::{Write, BufReader};
use std::time::Duration;
use std::sync::mpsc::channel;

use notify::{RecommendedWatcher, Watcher, RecursiveMode, DebouncedEvent};
use rustls;
use rustls::Session;
use webpki;

mod rfsn_proto;
use crate::rfsn_proto::{RFSNMsg, RFSNTest, RFSNFileHeader, RFSNDataChunk, RFSNEnd};

mod chunks;
use crate::chunks::Chunks;


// Certificate pinning
static SERVER_CERT: &'static [u8] = include_bytes!("../../certs/serv.pem.cert");


fn setup(watch_path:&Path, server_host_port:&str) {
    // Test watched path for existence
    if ! watch_path.exists() {
        panic!("Path does not exist");
    }
    
    // Change current working directory to which is being watched
    env::set_current_dir(&watch_path)
        .expect(&format!("Cannot cwd to path {}", watch_path.display()));
    
    // Test TCP+TLS connectivity to server
    let mut tls_session = setup_tls(server_host_port);
    let mut stream = TcpStream::connect(server_host_port)
        .expect("No connectivity to server");
    let mut stream = rustls::Stream::new(&mut tls_session, &mut stream);
    
    let test_pdu = RFSNTest::new().encode().unwrap();
    stream.write(&test_pdu).expect("Write failed on TCP/TLS stream");
    stream.flush().expect("Flush failed on TCP/TLS stream");
    
    // Output TLS params used
    let ciphersuite = tls_session.get_negotiated_ciphersuite().unwrap();
	let tls_version = tls_session.get_protocol_version().unwrap();
    println!("Using {:?} {:?}", tls_version, ciphersuite.suite);

    println!("Checks succeded: watched_path, tcp_connectivity");
}

fn setup_tls(server_host_port:&str) -> rustls::ClientSession {
    let hostname:&str = server_host_port.split(":").nth(0).unwrap();
    
    let mut config = rustls::ClientConfig::new();
    // BufRead required by signature, not generic enough to handle memory buffers
    let add_res = config.root_store.add_pem_file(&mut BufReader::new(SERVER_CERT));
    match add_res {
		Ok((0, _)) => panic!("Invalid PEM certificate {:02X?}", SERVER_CERT),
		_        => println!("Imported PEM certificate"),
		//_       => println!("Imported PEM certificate {:02X?} {:?}", SERVER_CERT, add_res),
	}
    
    // Setup TLS session
    let dns_name = webpki::DNSNameRef::try_from_ascii_str(hostname).unwrap();
	rustls::ClientSession::new(&Arc::new(config), dns_name)
}

fn watch(watch_path:&Path, server_host_port:&str) -> notify::Result<()> {
    // Create a channel to receive the events
    let (notify_tx, notify_rx) = channel();

    // Automatically select the best implementation for underlying platform
    let mut watcher:RecommendedWatcher = Watcher::new(notify_tx, Duration::from_secs(2))
        .expect("Filesystem watcher cannot be created");
    watcher.watch(watch_path, RecursiveMode::Recursive)
        .expect("Filesystem watcher cannot start");

    loop {
        let event = notify_rx.recv().expect("Watcher recieve error");
        println!("{:?}", event);
        match event {
            // If any type of file is written
            // Here pattern matching + match guard
            DebouncedEvent::Create(ref updated_path)
            | DebouncedEvent::Write(ref updated_path)
            if updated_path.is_file() =>
            {
                let mut updated_path:PathBuf = updated_path.canonicalize()
                    .unwrap();
                updated_path = updated_path.strip_prefix(&watch_path)
                    .unwrap().to_owned();
                println!("Relative path: {:?}", updated_path);
                
                send_server(&updated_path, server_host_port)
                    .unwrap_or_else(|e| println!("IO or protocol error: {:?}", e));
            },
            _ => ()
        }
    }
}

fn send_server(updated_path:&Path, server_host_port:&str) -> Result<(), Box<Error>> {
    // Setup TCP+TLS stream
    let mut tls_session = setup_tls(server_host_port);
    let mut stream = TcpStream::connect(server_host_port)?;
    let mut stream = rustls::Stream::new(&mut tls_session, &mut stream);
    
    // Open watched file
    let file = File::open(updated_path)?;
    
    println!("Connection established, file ready to send");
    
    rfsn_encode_and_send(&mut stream, &file, &updated_path)?;
    
    println!("Success: send_file");
    
    Ok(())
}

fn rfsn_encode_and_send(stream:&mut Write, file:&File, path:&Path) -> Result<(), Box<Error>> {
    // Get file size to send and tune sending
    let file_size = file.metadata()?.len();
    let buf_size = if file_size < 8*1024 { 1024 } else { 4*1024 };
    println!("File size {} ; Buffer size {}", file_size, buf_size);
    
    // Send metadata first
    let metadata_pdu = RFSNFileHeader::new(
        path.to_str().unwrap(),
        file_size,
    ).encode()?;
    stream.write(&metadata_pdu)?;
    stream.flush()?;
    
    // Read and send file chunk by chunk to have limited memory footprint
    for chunk_res in Chunks::new(file, buf_size).into_iter() {
        let chunk = chunk_res?;
        let data_pdu = RFSNDataChunk::new(&chunk).encode()?;
        
        stream.write(&data_pdu)?;
        stream.flush()?;
    }
    // Signal transfer end
    stream.write(&RFSNEnd::new().encode()?)?;
    stream.flush()?;

    Ok(())
}

fn main() {
    let path_arg = env::args().nth(1).expect("First argument must be the path to watch");
    let path = Path::new(&path_arg).canonicalize().unwrap();
    let server_host_port = env::args().nth(2).expect("Second argument must be the the server host:port");
    
    setup(&path, &server_host_port);
    
    if let Err(e) = watch(&path, &server_host_port) {
        panic!("Watch error: {:?}", e);
    }
}
